function [T1_new, T2_new]= make_christofell_symbols_change_variable(T1,T2,Jxy,Jyx,Hxy)

Txu_uu = T1(1,1);
Txv_uu = T2(1,1);
Txu_vv = T1(2,2);
Txv_vv = T2(2,2);
Txu_uv = T1(1,2);
Txv_uv = T2(1,2);
Txu_vu = Txu_uv;
Txv_vu = Txv_uv;

jxu_yu = Jxy(1,1);
jxu_yv = Jxy(1,2);
jxv_yu = Jxy(2,1);
jxv_yv = Jxy(2,2);

jyu_xu = Jyx(1,1);
jyu_xv = Jyx(1,2);
jyv_xu = Jyx(2,1);
jyv_xv = Jyx(2,2);

hxu_yuu = Hxy(1);
hxv_yuu = Hxy(2);
hxu_yuv = Hxy(3);
hxv_yuv = Hxy(4);
hxu_yvv = Hxy(5);
hxv_yvv = Hxy(6);

% christoffel symbols on y are given by the following change of variable

%Tk_ij = jxp_yi*jxq_yj*Txr_pq*jyk_xr + jyk_xm*hxm_yij
% resolving m and r gives
%Tk_ij = jxp_yi*jxq_yj*Txu_pq*jyk_xu + jxp_yi*jxq_yj*Txv_pq*jyk_xv  + jyk_xu*hxu_yij + jyk_xv*hxv_yij
% resolving p gives
%Tk_ij = jxu_yi*jxq_yj*Txu_uq*jyk_xu + jxv_yi*jxq_yj*Txu_vq*jyk_xu +...
%        jxu_yi*jxq_yj*Txv_uq*jyk_xv + jxv_yi*jxq_yj*Txv_vq*jyk_xv + jyk_xu*hxu_yij + jyk_xv*hxv_yij
% resolving q gives
% Tk_ij = jxu_yi*jxu_yj*Txu_uu*jyk_xu + jxu_yi*jxv_yj*Txu_uv*jyk_xu + jxv_yi*jxu_yj*Txu_vu*jyk_xu + jxv_yi*jxv_yj*Txu_vv*jyk_xu +...
%         jxu_yi*jxu_yj*Txv_uu*jyk_xv + jxu_yi*jxv_yj*Txv_uv*jyk_xv + jxv_yi*jxu_yj*Txv_vu*jyk_xv + jxv_yi*jxv_yj*Txv_vv*jyk_xv +...
%         jyk_xu*hxu_yij + jyk_xv*hxv_yij
    
Tu_uu = jxu_yu*jxu_yu*Txu_uu*jyu_xu + jxu_yu*jxv_yu*Txu_uv*jyu_xu + jxv_yu*jxu_yu*Txu_vu*jyu_xu + jxv_yu*jxv_yu*Txu_vv*jyu_xu +...
                 jxu_yu*jxu_yu*Txv_uu*jyu_xv + jxu_yu*jxv_yu*Txv_uv*jyu_xv + jxv_yu*jxu_yu*Txv_vu*jyu_xv + jxv_yu*jxv_yu*Txv_vv*jyu_xv +...
                 jyu_xu*hxu_yuu + jyu_xv*hxv_yuu ;

Tv_uu = jxu_yu*jxu_yu*Txu_uu*jyv_xu + jxu_yu*jxv_yu*Txu_uv*jyv_xu + jxv_yu*jxu_yu*Txu_vu*jyv_xu + jxv_yu*jxv_yu*Txu_vv*jyv_xu +...
                 jxu_yu*jxu_yu*Txv_uu*jyv_xv + jxu_yu*jxv_yu*Txv_uv*jyv_xv + jxv_yu*jxu_yu*Txv_vu*jyv_xv + jxv_yu*jxv_yu*Txv_vv*jyv_xv +...
                 jyv_xu*hxu_yuu + jyv_xv*hxv_yuu;
    
Tu_vv = jxu_yv*jxu_yv*Txu_uu*jyu_xu + jxu_yv*jxv_yv*Txu_uv*jyu_xu + jxv_yv*jxu_yv*Txu_vu*jyu_xu + jxv_yv*jxv_yv*Txu_vv*jyu_xu +...
                 jxu_yv*jxu_yv*Txv_uu*jyu_xv + jxu_yv*jxv_yv*Txv_uv*jyu_xv + jxv_yv*jxu_yv*Txv_vu*jyu_xv + jxv_yv*jxv_yv*Txv_vv*jyu_xv +...
                 jyu_xu*hxu_yvv + jyu_xv*hxv_yvv ;
    
Tv_vv = jxu_yv*jxu_yv*Txu_uu*jyv_xu + jxu_yv*jxv_yv*Txu_uv*jyv_xu + jxv_yv*jxu_yv*Txu_vu*jyv_xu + jxv_yv*jxv_yv*Txu_vv*jyv_xu +...
                 jxu_yv*jxu_yv*Txv_uu*jyv_xv + jxu_yv*jxv_yv*Txv_uv*jyv_xv + jxv_yv*jxu_yv*Txv_vu*jyv_xv + jxv_yv*jxv_yv*Txv_vv*jyv_xv +...
                 jyv_xu*hxu_yvv + jyv_xv*hxv_yvv ;

Tu_uv = jxu_yu*jxu_yv*Txu_uu*jyu_xu + jxu_yu*jxv_yv*Txu_uv*jyu_xu + jxv_yu*jxu_yv*Txu_vu*jyu_xu + jxv_yu*jxv_yv*Txu_vv*jyu_xu +...
                 jxu_yu*jxu_yv*Txv_uu*jyu_xv + jxu_yu*jxv_yv*Txv_uv*jyu_xv + jxv_yu*jxu_yv*Txv_vu*jyu_xv + jxv_yu*jxv_yv*Txv_vv*jyu_xv +...
                 jyu_xu*hxu_yuv + jyu_xv*hxv_yuv ;
    
Tv_uv = jxu_yu*jxu_yv*Txu_uu*jyv_xu + jxu_yu*jxv_yv*Txu_uv*jyv_xu + jxv_yu*jxu_yv*Txu_vu*jyv_xu + jxv_yu*jxv_yv*Txu_vv*jyv_xu +...
                 jxu_yu*jxu_yv*Txv_uu*jyv_xv + jxu_yu*jxv_yv*Txv_uv*jyv_xv + jxv_yu*jxu_yv*Txv_vu*jyv_xv + jxv_yu*jxv_yv*Txv_vv*jyv_xv +...
                 jyv_xu*hxu_yuv + jyv_xv*hxv_yuv ;

Tu_vu = Tu_uv;
Tv_vu = Tv_uv;

T1_new = [Tu_uu Tu_uv; Tu_vu Tu_vv];
T2_new = [Tv_uu Tv_uv; Tv_vu Tv_vv];