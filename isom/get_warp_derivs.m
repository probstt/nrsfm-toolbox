function [ q_n, J21, Huu21, Huv21, Hvv21] = get_warp_derivs( m )
% function to get warp derivatives for shaifali's method

n = length(m); % number of frames/images

% do not remove points without ground truth.

% put points in cell array
q_n = cell(1,n);

for k = 1: n    
    q_n{k} = m{k}.m;
end

% create bbs warps:

er= 1e-4;
J21 = cell(1,n-1);
Huu21 = cell(1,n-1);
Huv21 = cell(1,n-1);
Hvv21 = cell(1,n-1);
for i = 2:n
    umin = min(q_n{i}(1,:))-t; umax = max(q_n{i}(1,:))+t;
    vmin = min(q_n{i}(2,:))-t; vmax = max(q_n{i}(2,:))+t;
    bbs = bbs_create(umin, umax, nC, vmin, vmax, nC, 2);
    coloc = bbs_coloc(bbs, q_n{i}(1,:), q_n{i}(2,:));
    lambdas = er*ones(nC-3, nC-3);
    bending = bbs_bending(bbs, lambdas);
    cpts = (coloc'*coloc + bending) \ (coloc'*q_n{1}');
    ctrlpts = cpts';
    [xv,yv]=meshgrid(linspace(bbs.umin,bbs.umax,100),linspace(bbs.vmin,bbs.vmax,100));
    ctrlpts = optimPanalSchwarz(bbs,ctrlpts,q_n{i}',q_n{1}',[xv(:),yv(:)],par(i));
    ctrlpts=ctrlpts';
    qw2{i-1} = bbs_eval(bbs,ctrlpts,q_n{i}(1,:)',q_n{i}(2,:)',0,0);
    error=sqrt(mean((qw2{i-1}(1,:)-q_n{1}(1,:)).^2+(qw2{i-1}(2,:)-q_n{1}(2,:)).^2));
    dqu = bbs_eval(bbs, ctrlpts, q_n{i}(1,:)',q_n{i}(2,:)',1,0);
    dqv = bbs_eval(bbs, ctrlpts, q_n{i}(1,:)',q_n{i}(2,:)',0,1);
    dquv = bbs_eval(bbs,ctrlpts,q_n{i}(1,:)',q_n{i}(2,:)',1,1);
    dquu = bbs_eval(bbs, ctrlpts, q_n{i}(1,:)',q_n{i}(2,:)',2,0);
    dqvv = bbs_eval(bbs, ctrlpts, q_n{i}(1,:)',q_n{i}(2,:)',0,2);
    J21{i-1} = [dqu; dqv];
    Huu21{i-1} = dquu;
    Huv21{i-1} = dquv;
    Hvv21{i-1} = dqvv;
%     err = check_shwarz(J21{i-1}',Huu21{i-1}',Huv21{i-1}',Hvv21{i-1}');
%     mean(abs(err))
    disp([sprintf('[ETA] Internal Rep error = %f',error)]);
%     %Visualize Point Registration Error
%     [xv,yv]=meshgrid(linspace(bbs.umin,bbs.umax,20),linspace(bbs.vmin,bbs.vmax,20));
%     qv = bbs_eval(bbs,ctrlpts,xv(:),yv(:),0,0);
%     figure;
%     plot(q{1}(1,:),q{1}(2,:),'ro');
%     hold on;
%     plot(qw2{i-1}(1,:),qw2{i-1}(2,:),'b*');
%     mesh(reshape(qv(1,:),size(xv)),reshape(qv(2,:),size(xv)),zeros(size(xv)));
%     axis equal
%     hold off;
end

end

