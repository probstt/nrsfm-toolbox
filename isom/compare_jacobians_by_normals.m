function [err,n,n2] = compare_jacobians_by_normals(J_phi,J_res)
 nu = J_phi(1:3);
 nu = nu./norm(nu);
 nv = J_phi(4:6);
 nv = nv./norm(nv);
 n = -cross(nu,nv);
 n = n./norm(n);
 
 nu = J_res(1:3,1);
 nu = nu./norm(nu);
 nv = J_res(1:3,2);
 nv = nv./norm(nv);
 n2 = -cross(nu,nv);
 n2 = n2./norm(n2);
[err idx] = min([acosd(dot(n,n2)),acosd(dot(n,-n2))]);
if idx==2
    n2=-n2;
end
    