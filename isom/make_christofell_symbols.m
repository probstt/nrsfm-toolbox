function [T1, T2]= make_christofell_symbols(k1,k2)

T1 = [-2*k1 -k2;-k2 0];
T2 = [0 -k1;-k1 -2*k2];
end