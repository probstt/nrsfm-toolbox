function [J12,J21,Huu21,Huv21,Hvv21,qw2,err]=get_derivatives_s(Hmo,m)

 for i=2:length(m)
    
    H_obt = Hmo(i,1).H; % homography at 0,0
    H_obt = H_obt./repmat(H_obt(9,:),9,1);
    a = H_obt(1,:) ;
    b = H_obt(4,:);
    c = H_obt(7,:);
    d = H_obt(3,:);
    e = H_obt(6,:);
    g = H_obt(2,:);
    h = H_obt(5,:);
    k = H_obt(8,:);
    f = H_obt(9,:);
    
    X = a.*m{i}(1,:) + b.*m{i}(2,:) + c;
    Y = g.*m{i}(1,:) + h.*m{i}(2,:) + k;
    Z = d.*m{i}(1,:) + e.*m{i}(2,:) + f;

qw2{i-1}= [X./Z;Y./Z];
    error=sqrt(mean((qw2{i-1}(1,:)-m{1}(1,:)).^2+(qw2{i-1}(2,:)-m{1}(2,:)).^2));

    J21{i-1} = [(a.*Z -X.*d)./(Z.^2);(g.*Z -Y.*d)./(Z.^2);(b.*Z -X.*e)./(Z.^2);(h.*Z-Y.*e)./(Z.^2)];
    Huu21{i-1} = [2.*d.*(X.*d-a.*Z)./(Z.^3);2.*d.*(Y.*d-g.*Z)./(Z.^3)];
    Huv21{i-1} = [(2.*X.*d.*e -(a.*e + b.*d).*Z)./(Z.^3);(2.*Y.*d.*e -(g.*e + h.*d).*Z)./(Z.^3)];
    Hvv21{i-1} = [2.*e.*(X.*e-b.*Z)./(Z.^3);2.*e.*(Y.*e-h.*Z)./(Z.^3)];
    err{i-1} = check_shwarz(J21{i-1}',Huu21{i-1}',Huv21{i-1}',Hvv21{i-1}');
    %disp([sprintf('[ETA] Internal Rep error = %f',error)]);
    %Visualize Point Registration Error

%     figure;
%     plot(m{1}(1,:),m{1}(2,:),'ro');
%     hold on;
%     plot(qw2{i-1}(1,:),qw2{i-1}(2,:),'b*');
%     axis equal
%     hold off;
 end

 for i=2:length(m)
    
    H_obt = Hmo(1,i).H; % homography at 0,0
    H_obt = H_obt./repmat(H_obt(9,:),9,1);
    a = H_obt(1,:) ;
    b = H_obt(4,:);
    c = H_obt(7,:);
    d = H_obt(3,:);
    e = H_obt(6,:);
    g = H_obt(2,:);
    h = H_obt(5,:);
    k = H_obt(8,:);
    f = H_obt(9,:);
    
    X = a.*m{i}(1,:) + b.*m{i}(2,:) + c;
    Y = g.*m{i}(1,:) + h.*m{i}(2,:) + k;
    Z = d.*m{i}(1,:) + e.*m{i}(2,:) + f;

    J12{i-1} = [(a.*Z -X.*d)./(Z.^2);(g.*Z -Y.*d)./(Z.^2);(b.*Z -X.*e)./(Z.^2);(h.*Z-Y.*e)./(Z.^2)];
   
    
end