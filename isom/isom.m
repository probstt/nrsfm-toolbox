% Parashar, S., Pizarro, D., Bartoli, A.: Isometric non-rigid shape-from-motion in linear time. In: CVPR. (2016)
function [ data ] = isom(data,opts)

%Get data
for k=1:length(data)
   mMat(k).m=data(k).x2d(1:2,:); 
end

N = length(mMat(1).m);
M = length(mMat);

%Get parameters
par = (opts.schwarzian)*ones(1,M); % schwarzian parameter.. needs to be tuned (usually its something close to 1e-3)
Qd = run_shaifali_method(mMat,par);

for k=1:M
    data(k).x3d=Qd{k};
end

end