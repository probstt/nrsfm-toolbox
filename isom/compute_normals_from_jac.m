function [ n2 ] = compute_normals_from_jac( J_res )
% compute normals from the jacobian and disambiguate sign

nu = J_res(1:3,1);
nu = nu./norm(nu);
nv = J_res(1:3,2);
nv = nv./norm(nv);
n2 = -cross(nu,nv);
n2 = n2./norm(n2);

if n2(3) > 0
    n2=-n2;
end

end