function [Qdreg]=get_depth_syn2(N_res,q,par)
nC=100;
lambdas = par*ones(nC-3, nC-3);
n = length(q);
Qdreg = cell(1,n);
for i=1:length(q)
    umin=min(q{i}(1,:))-0.1;umax=max(q{i}(1,:))+0.1;
    vmin=min(q{i}(2,:))-0.1;vmax=max(q{i}(2,:))+0.1;
    bbsd = bbs_create(umin, umax, nC, vmin, vmax, nC, 1);
    colocd = bbs_coloc(bbsd, q{i}(1,:), q{i}(2,:));
    bendingd = bbs_bending(bbsd, lambdas);
    [ctrlpts3Dn]=ShapeFromNormals(bbsd,colocd,bendingd,[q{i};ones(1,length(q{i}))],N_res{i});
    % [ctrlpts3Dn]=ShapeFromNormalsL1(bbsd,colocd,bendingd,m1p,n1p);
    mu=bbs_eval(bbsd, ctrlpts3Dn, q{i}(1,:)', q{i}(2,:)',0,0);
    % figure(1);
    % plot3(m1(1,:).*mu,m1(2,:).*mu,mu,'bo');
    % axis equal;
    
    Qd = zeros(3,size(q{i},2));
    Qd(1,:) = q{i}(1,:).*mu;
    Qd(2,:) = q{i}(2,:).*mu;
    Qd(3,:) = mu;
    Qdreg{i} = Qd;
end
