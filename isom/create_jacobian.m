function J = create_jacobian(k1,k2,u,v)
J= [1-k1*u, -k2*u;-k1*v, 1-k2*v;-k1,-k2];