n% for PLANES
% test iso-nrsfm in varrious conditions
clear all;
close all;

addpath('BBS');
addpath('SfTv0_3');
addpath('globtipoly');
addpath(genpath('SeDuMi_1_3'));

load syn_surfaces_warps.mat
%load tshirt_warps_4.mat
%load hulk_warps_4.mat
%load tshirt_ajad4.mat

%%
 H = nviewsrealHomography2s(q,bbs,ctrlpts);
 H = computeglobalHomography(H);
 
 [J21,Huu21,Huv21,Hvv21,q21,err_w]=get_derivatives_s(H,q);

% globtipoly refinement
error =[];
for i= 1: length(q{1})
    mpol k1 k2;
    [T1, T2]= make_christofell_symbols(k1,k2);
    G = make_metric_tensor_G(k1,k2,q{1}(1,i),q{1}(2,i));
    pol = 0;
    for j= 1:num-1
        Jxy = reshape(J21{j}(:,i),2,2);
        Jyx = pinv(Jxy);
        Hxy = [Huu21{j}(:,i);Huv21{j}(:,i);Hvv21{j}(:,i)];
        [T1_new, T2_new]= make_christofell_symbols_change_variable(T1,T2,Jxy,Jyx,Hxy);
        k1_bar = -T1_new(1,1)/2;
        k2_bar = -T2_new(2,2)/2;
        % make k1_bar, k2_bar from T1_new and T2_new (we are gonna use schwarps for this)
        Gnew_bar = make_metric_tensor_G(k1_bar,k2_bar,q{j+1}(1,i),q{j+1}(2,i));
        Gnew = make_metric_tensor_pullback(G,Jxy);
        
        L1 = Gnew_bar(1,1)*Gnew(1,2) - Gnew_bar(1,2)*Gnew(1,1);
        L2 = Gnew_bar(1,1)*Gnew(2,2) - Gnew_bar(2,2)*Gnew(1,1);
        %L3 = Gnew_bar(2,2)*Gnew(1,2) - Gnew_bar(1,2)*Gnew(2,2);
        pol = pol + L1^2 + L2^2;% + L3^2;
    end
    P = msdp(min(pol));
    [status,obj] = msol(P,7);
    if status > 0
    res = double([k1 k2]);
    J_res = create_jacobian(res(1),res(2),q{1}(1,i),q{1}(2,i));
    [err{1}(i), N_phi{1}(:,i), N_res{1}(:,i)] = compare_jacobians_by_normals(jac{1}(:,i),J_res);
    % check if the new christoffel symbols calculated are correct
    [T1, T2]= make_christofell_symbols(res(1),res(2));
    for j= 1:num-1
        Jxy = reshape(J21{j}(:,i),2,2);
        Jyx = pinv(Jxy);
        Hxy = [Huu21{j}(:,i);Huv21{j}(:,i);Hvv21{j}(:,i)];
        
        [T1_new, T2_new]= make_christofell_symbols_change_variable(T1,T2,Jxy,Jyx,Hxy);
        error=[error,[T1_new(1,1)-2*T2_new(1,2),T2_new(2,2)-2*T1_new(1,2),T1_new(2,2),T2_new(1,1)]'];
        k1_bar = -T1_new(1,1)/2;
        k2_bar = -T2_new(2,2)/2;
        J_res = create_jacobian(k1_bar,k2_bar,q{j+1}(1,i),q{j+1}(2,i));
        [err{j+1}(i), N_phi{j+1}(:,i), N_res{j+1}(:,i)] = compare_jacobians_by_normals(jac{j+1}(:,i),J_res);
    end
    clear k1 k2 res
    end
    
end

for i=1:length(q)
    figure;
    hold on
    axis equal
    plot3(P2{i}(1,:),P2{i}(2,:),P2{i}(3,:),'ro');
    h=quiver3(P2{i}(1,:),P2{i}(2,:),P2{i}(3,:),N_phi{i}(1,:),N_phi{i}(2,:),N_phi{i}(3,:),1);
    set(h,'Color',[0,0,1]);
    h=quiver3(P2{i}(1,:),P2{i}(2,:),P2{i}(3,:),N_res{i}(1,:),N_res{i}(2,:),N_res{i}(3,:),1);
    set(h,'Color',[1,0,0]);
    legend({'Surface Points','Nphi','Nres'})
    hold off
    
    mean(err{i}) % in degrees
%     max(max(error))
%     mean(mean(error))
end
