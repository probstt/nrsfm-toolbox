function [N_res, err]= get_shape_error(q,Qreg,N_phi)

for i=1 :length(q)
    n1gth=N_phi{i};
    m1 = q{i};
    n= 40;
    % Get normals for the reconstruction
    nC = 30;
    umin=min(m1(1,:))-0.1;umax=max(m1(1,:))+0.1;
    vmin=min(m1(2,:))-0.1;vmax=max(m1(2,:))+0.1;
    bbs3D = bbs_create(umin, umax, nC, vmin, vmax, nC, 3);
    coloc3D = bbs_coloc(bbs3D, m1(1,:), m1(2,:));
    lambdas = 1e-4*ones(nC-3, nC-3);
    bending3D = bbs_bending(bbs3D, lambdas);
    ctrlpts3D = (coloc3D'*coloc3D + bending3D) \ (coloc3D'*Qreg{i}');
    ctrlpts3D = ctrlpts3D';
    
    n1x=bbs_eval(bbs3D, ctrlpts3D, m1(1,:)', m1(2,:)',1,0);
    n1y=bbs_eval(bbs3D, ctrlpts3D, m1(1,:)', m1(2,:)',0,1);
    n1v=zeros(3,length(m1(1,:)));
    
    for k=1:size(n1x,2)
        nx=n1x(:,k);
        nx=nx./norm(nx);
        ny=n1y(:,k);
        ny=ny./norm(ny);
        nn=-cross(nx,ny);
        nn=nn./norm(nn);
        n1v(:,k)=nn;
        %n1(:,i)=-sign(nn).*abs(n1(:,i));
    end
    
    n1p = n1v;
    % Shape error
    errorv=zeros(1,size(n1gth,2));
    for j=1:size(n1gth,2)
        errorv(j)=acos(n1gth(:,j)'*n1p(:,j)).*180/pi;
    end
    err{i}= mean(errorv);
    N_res{i}= n1p;
end
