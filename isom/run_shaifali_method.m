function [ Qd ] = run_shaifali_method( m, par, visib)

% function to run shaifali's method given a structure of image points- m.
N = length(m(1).m);
M = length(m);
if nargin < 3
    visib = ones(N,M);
end


% inputs: m is a structure        
n = length(m); % number of frames/images
q_n = cell(1,n);
for k = 1: length(m)
    q_n{k} = m(k).m(1:2,:);
end

%METHOD 1 : MY METHOD
% warps are corrected using schwarzians    

% [J21,Huu21,Huv21,Hvv21,~] = create_warps(q_n, par);
[J21,Huu21,Huv21,Hvv21] = create_warps_missing(q_n, par, visib);
% normals
N_res = gloptipoly_refinement2(q_n,J21,Huu21,Huv21,Hvv21);
%  depths and 3D
Qd = get_depth_syn2(N_res,q_n,1e0);

end