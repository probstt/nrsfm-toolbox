syms real  u0 v0 x0 y0 x y u v r dxu dxv dyu dyv dxuu dxuv dxvv dxvu dux duy dvx dvy duxx duxy dvxx dxyy
syms guv guu gvu gvv gxy gxx gyx gyy gxx_x gyy_x gxy_x gyx_x gxx_y gxy_y gyx_y gyy_y g_xx g_xy g_yx g_yy

x= tan(u0/r);
y= v0/(r*cos(u0/r));
u = r*atan(x0);
v = r*y0/sqrt(1+x0^2);

dux = diff(u,x0);
duxx = diff(dux,x0);
duxy = diff(dux,y0);
duy = diff(u,y0);
duyx = diff(duy,x0);
duyy = diff(duy,y0);

dvx = diff(v,x0);
dvxx = diff(dvx,x0);
dvxy = diff(dvx,y0);
dvy = diff(v,y0);
dvyx = diff(dvy,x0);
dvyy = diff(dvy,y0);

dxu = diff(x,u0);
dxuu = diff(dxu,u0);
dxuv = diff(dxu,v0);
dxv = diff(x,v0);
dxvu = diff(dxv,u0);
dxvv = diff(dxv,v0);

dyu = diff(y,u0);
dyuu = diff(dyu,u0);
dyuv = diff(dyu,v0);
dyv = diff(y,v0);
dyvu = diff(dyv,u0);
dyvv = diff(dyv,v0);

guu =1;
gvv =1;
guv =0;
gvu =0;

gxx = simplify(guu*dux*dux + gvv*dvx*dvx);
gyy = simplify(guu*duy*duy + gvv*dvy*dvy);
gxy = simplify(guu*dux*duy + gvv*dvx*dvy);
gyx = gxy;

g_xx = simplify(gyy/(gxx*gyy-gxy*gyx));
g_yy = simplify(gxx/(gxx*gyy-gxy*gyx));
g_xy = simplify(-gxy/(gxx*gyy-gxy*gyx));
g_yx = simplify(-gyx/(gxx*gyy-gxy*gyx));

gxx_x = simplify(diff(gxx,x0));
gyy_x = simplify(diff(gyy,x0));
gxy_x = simplify(diff(gxy,x0));
gyx_x = gxy_x;

gxx_y = simplify(diff(gxx,y0));
gyy_y = simplify(diff(gyy,y0));
gxy_y = simplify(diff(gxy,y0));
gyx_y = gxy_y;

% Method 1
%Tij_m = 0.5*g_ml(gil_j + glj_i -gij_l)


Txx_x = simplify(0.5*g_xx*(gxx_x + gxx_x -gxx_x) + 0.5*g_xy*(gxy_x + gyx_x -gxx_y))
Txx_y = simplify(0.5*g_yx*(gxx_x + gxx_x -gxx_x) + 0.5*g_yy*(gxy_x + gyx_x -gxx_y))
Tyy_x = simplify(0.5*g_xx*(gyx_y + gxy_y -gyy_x) + 0.5*g_xy*(gyy_y + gyy_y -gyy_y))
Tyy_y = simplify(0.5*g_yx*(gyx_y + gxy_y -gyy_x) + 0.5*g_yy*(gyy_y + gyy_y -gyy_y))
Txy_x = simplify(0.5*g_xx*(gxx_y + gxy_x -gxy_x) + 0.5*g_xy*(gxy_y + gyy_x -gxy_y))
Txy_y = simplify(0.5*g_yx*(gxx_y + gxy_x -gxy_x) + 0.5*g_yy*(gxy_y + gyy_x -gxy_y))
Tyx_x = Txy_x;
Tyx_y = Txy_y;


% Method 2
%Tjk_i = drjk*dir

T1xx_x = simplify(duxx*dxu + dvxx*dxv);
T1xx_y = simplify(duxx*dyu + dvxx*dyv);
T1yy_x = simplify(duyy*dxu + dvyy*dxv);
T1yy_y = simplify(duyy*dyu + dvyy*dyv);
T1xy_x = simplify(duxy*dxu + dvxy*dxv);
T1xy_y = simplify(duxy*dyu + dvxy*dyv);
T1yx_x = T1xy_x;
T1yx_y = T1xy_y;

% covariant derivative
Ax_x = g;
Ax_y =;
Ay_x =;
Ay_y =;

