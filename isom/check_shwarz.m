function err = check_shwarz(J21,Huu21,Huv21,Hvv21)
err=[];
for i=1:length(J21)
    err(i,:) = [Huu21(i,1)*J21(i,2)-Huu21(i,2)*J21(i,1);Hvv21(i,1)*J21(i,4)-Hvv21(i,2)*J21(i,3);...
                Huu21(i,1)*J21(i,4)-Huu21(i,2)*J21(i,3) + 2*(Huv21(i,1)*J21(i,2)-Huv21(i,2)*J21(i,1));...
                Hvv21(i,1)*J21(i,2)-Hvv21(i,2)*J21(i,1) + 2*(Huv21(i,1)*J21(i,4)-Huv21(i,2)*J21(i,3))];
end
