function [err_s, N_phi, N_res] = gloptipoly_refinement(q,J21,Huu21,Huv21,Hvv21,num,jac)

% globtipoly refinement
error =[];
%iso_sum = zeros(length(q),length(q{1}));
for i= 1: length(q{1})
    i
    mpol k1 k2;
    [T1, T2]= make_christofell_symbols(k1,k2);
    G = make_metric_tensor_G(k1,k2,q{1}(1,i),q{1}(2,i));
    pol = 0;
    for j= 1:num-1
        Jxy = reshape(J21{j}(:,i),2,2);
        Jyx = pinv(Jxy);%
        Hxy = [Huu21{j}(:,i);Huv21{j}(:,i);Hvv21{j}(:,i)];
        [T1_new, T2_new]= make_christofell_symbols_change_variable(T1,T2,Jxy,Jyx,Hxy);
        k1_bar = -T1_new(1,1)/2;
        k2_bar = -T2_new(2,2)/2;
        % make k1_bar, k2_bar from T1_new and T2_new (we are gonna use schwarps for this)
        Gnew_bar = make_metric_tensor_G(k1_bar,k2_bar,q{j+1}(1,i),q{j+1}(2,i));
        Gnew = make_metric_tensor_pullback(G,Jxy);
        
        L1 = Gnew_bar(1,1)*Gnew(1,2) - Gnew_bar(1,2)*Gnew(1,1);
        L2 = Gnew_bar(2,2)*Gnew(1,2) - Gnew_bar(1,2)*Gnew(2,2);
%         L1 = Jyx(1,:)*Gnew_bar*Jyx(2,:)'*Gnew(1,1) - Jyx(1,:)*Gnew_bar*Jyx(1,:)'*Gnew(1,2);
%         L2 = Jyx(2,:)*Gnew_bar*Jyx(2,:)'*Gnew(1,1) - Jyx(1,:)*Gnew_bar*Jyx(1,:)'*Gnew(2,2);
        %L3 = Gnew_bar(2,2)*Gnew(1,2) - Gnew_bar(1,2)*Gnew(2,2);
        pol = pol + L1^2 + L2^2;% + L3^2;
    end
   
    P = msdp(min(pol));
 
    [status,obj] = msol(P,10);
    if status > 0
    res = double([k1 k2]);
    J_res = create_jacobian(res(1),res(2),q{1}(1,i),q{1}(2,i));
    %iso_sum(1,i) = iso_sum(1,i) + norm(J_res'*J_res - eye(2));
    
    [err{1}(i), N_phi{1}(:,i), N_res{1}(:,i)] = compare_jacobians_by_normals(jac{1}(:,i),J_res);
    % check if the new christoffel symbols calculated are correct
    [T1, T2]= make_christofell_symbols(res(1),res(2));
    for j= 1:num-1
        Jxy = reshape(J21{j}(:,i),2,2);
        Jyx = pinv(Jxy);%
        Hxy = [Huu21{j}(:,i);Huv21{j}(:,i);Hvv21{j}(:,i)];
        
        [T1_new, T2_new]= make_christofell_symbols_change_variable(T1,T2,Jxy,Jyx,Hxy);
        error=[error,[T1_new(1,1)-2*T2_new(1,2),T2_new(2,2)-2*T1_new(1,2),T1_new(2,2),T2_new(1,1)]'];
        k1_bar = -T1_new(1,1)/2;
        k2_bar = -T2_new(2,2)/2;
        J_res = create_jacobian(k1_bar,k2_bar,q{j+1}(1,i),q{j+1}(2,i));
        % iso_sum(j+1,i) = iso_sum(j+1,i) + norm(J_res'*J_res - eye(2));
        [err{j+1}(i), N_phi{j+1}(:,i), N_res{j+1}(:,i)] = compare_jacobians_by_normals(jac{j+1}(:,i),J_res);
    end
    clear k1 k2 res obj status
    end
    
   
    
end
for i=1:length(err)
    err_s{i}= mean(err{i});
end