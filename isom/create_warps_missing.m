function [J21,Huu21,Huv21,Hvv21] = create_warps_missing(q_n, par, visib)

n = length(q_n); % number of frames
visib = logical(visib);
% make invisible q_n into NaNs first
q_o = q_n; % final
for k = 1:n
   q_n{k}(1,~visib(:,k)) = NaN;
   q_n{k}(2,~visib(:,k)) = NaN;
end

% remove points where q_n{1} are NaNs
q_no = cell(1,n); % original

for k = 1: n
    q_no{k} = q_n{k}(:,visib(:,1));    
end
t = 1e-3; % extra margin for domain
nC = 24;
er= 1e-4;
% hallucinate correspondences on all visible and invisible points
for k = 2: n
    %
    id = isnan(q_no{k}(1,:));
    q1 = q_no{1}; q1(:,id)=[]; q2 = q_no{k}; q2(:,id) = [];
    umin = min(q_o{1}(1,:))-t; umax = max(q_o{1}(1,:))+t;
    vmin = min(q_o{1}(2,:))-t; vmax = max(q_o{1}(2,:))+t;
    bbs = bbs_create(umin, umax, nC, vmin, vmax, nC, 2);
    coloc = bbs_coloc(bbs, q1(1,:), q1(2,:));
    lambdas = er*ones(nC-3, nC-3);
    bending = bbs_bending(bbs, lambdas);
    cpts = (coloc'*coloc + bending) \ (coloc'*q2');
    ctrlpts = cpts';    
    q_o{k}(:,~visib(:,k)) = bbs_eval(bbs,ctrlpts,q_o{1}(1,~visib(:,k)),q_o{1}(2,~visib(:,k)),0,0);    
end

[J21,Huu21,Huv21,Hvv21,~] = create_warps(q_o, par);


end