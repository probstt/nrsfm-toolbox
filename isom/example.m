% example script
clear all;close all;

addpath('BBS');
addpath('SfTv0_3');
addpath('gloptipoly3');
addpath('SeDuMi_1_3');

load tshirt_for_shaifali.mat
K = scene.K;
idx3 = [ 1 2 3 4 5 6 7 8 9 10]; % the images the algorithm needs to be evaluated on (in order)
par = 2e-3.*ones(1,10); % schwarzian parameter.. needs to be tuned (usually its something close to 1e-3)

[P2, q,jac,J21_n,Huu21_n,Huv21_n,Hvv21_n,N_phi_n] = create_tshirt_dataset(idx3,10, scene, par);
for num = 5:5%3:10
    P2_n = P2(1:num);
    q_n = q(1:num);
    jac_n = jac(1:num);
    J21 = J21_n(1:num-1);
    Huu21 = Huu21_n(1:num-1);
    Huv21 = Huv21_n(1:num-1);
    Hvv21 = Hvv21_n(1:num-1);
    N_phi = N_phi_n(1:num);
    idx = idx3(1:num);
    %METHOD 1 : MY METHOD
    % warps are corrected using schwarzians
    [J21,Huu21,Huv21,Hvv21,q21] = create_warps(P2_n,q_n,num,par);
    % shape error
    [err_s1{num-2}, N_phi, N_res1{num-2}] = gloptipoly_refinement(q_n,J21,Huu21,Huv21,Hvv21,num,jac_n);
    %     %depth error
    [Q21{num-2},err_d1{num-2}]=get_depth_syn(N_res1{num-2},q_n,P2_n,1e0);
    [N_res12{num-2}, err_s12{num-2}]= get_shape_error(q_n,Q21{num-2},N_phi);
end
save('example_tshirt_ajad2.mat')
