function Gnew = make_metric_tensor_pullback(G,Jxy)

jxu_yu = Jxy(1,1);
jxu_yv = Jxy(1,2);
jxv_yu = Jxy(2,1);
jxv_yv = Jxy(2,2);

Gnew = [G(1,1)*jxu_yu*jxu_yu + G(1,2)*jxu_yu*jxv_yu + G(2,1)*jxv_yu*jxu_yu + G(2,2)*jxv_yu*jxv_yu,...
        G(1,1)*jxu_yu*jxu_yv + G(1,2)*jxu_yu*jxv_yv + G(2,1)*jxv_yu*jxu_yv + G(2,2)*jxv_yu*jxv_yv;...
        G(1,1)*jxu_yv*jxu_yu + G(1,2)*jxu_yv*jxv_yu + G(2,1)*jxv_yv*jxu_yu + G(2,2)*jxv_yv*jxv_yu,...
        G(1,1)*jxu_yv*jxu_yv + G(1,2)*jxu_yv*jxv_yv + G(2,1)*jxv_yv*jxu_yv + G(2,2)*jxv_yv*jxv_yv];