% Ji, P., Li, H., Dai, Y., Reid, I.: ”Maximizing Rigidity” revisited: A convex programming
% approach for generic 3d shape reconstruction from multiple perspective views. In: ICCV. (2017)
%
%
function data=MaxRig(data,opts)

%Get data
for k=1:length(data)
   mMat(k).m=data(k).x2d; 
end

%Get parameters
Kneighbors = opts.Kneighbors;
lambda1 = opts.lambda1;
lambda2 = opts.lambda2 ;

N = length(mMat(1).m);
M = length(mMat);

visibt = true(N,M);
[IDX, dist_sort] = getNeighborsVis(mMat,Kneighbors,visibt);

C = getAngleCos(mMat,IDX);


% second part: formulate SDP and solve with CVX
disp('NRSfM function');
tic;
[mu,D] = OurNRSfM(IDX,C, mMat, lambda1,lambda2);
ts = toc;

%% Fill return variable
for k=1:length(data)
    data(k).x3d=repmat(reshape(mu(k,:)',1,N),[3,1]).*mMat(k).m;
end