
%% Setup solvers
setup()

%% Set reconstruction parameters
options=getDefaultOptions();

%% Load dataset

opts.sv=1;
opts.sp=1;
opts.normalizeK=1;

% Hulk example
% dataset=prepare_hulk(opts);

% Flag example
% dataset=prepare_flag(opts);

% Tshirt
dataset=prepare_tshirt(opts);

data=dataset.data;
imSize=dataset.imageSize;
N=dataset.N;
M=dataset.M;

template=dataset.template;

%% Reconstruct
rec=template_mdh(data,template);


%% Evaluate
[res, ~, err3d]=evaluate(rec,1:M,options.template_mdh);
mean(err3d)


%% Visualize
figure(1)   
visualize(res,1:M);





