function [mu,D]=NrSfM_c2f_v(IDX,mMat,visib)

N = size(mMat,2);
M = size(mMat,3);
L = size(IDX,2);
vM = 1:M;

import mosek.fusion.*;
Mdl = Model('trustMeYouAreAwesome');
% accs=Mdl.getAcceptedSolutionStatus;
% Mdl.acceptedSolutionStatus(accs.Anything)
Z = Mdl.variable('Z', [M,N], Domain.greaterThan(0.0));
D = Mdl.variable('D', [L-1,N],Domain.greaterThan(0.0));

for i = 1:N % for each point
    diV  =  Var.flatten(Var.repeat(D.slice([1,i],[L,i+1]),1, M));
    ziV3 =  Var.repeat(Var.repeat(Z.slice([1,i],[M+1,i+1]),L-1),1,3);    
    mID  = repmat(IDX(i,2:end),M,1);
    idxN = [repmat(vM',L-1,1), mID(:)];
    xiML = repmat(permute(mMat(:,i,:),[1,3,2])',L-1,1);
    xjML = reshape(permute(mMat(:,IDX(i,2:end),:),[1,3,2]),3,M*(L-1))';
    
    vis_i=visib(ones(1,L-1)*i,:);
    vis_j=visib(IDX(i,2:end),:);
    vis=find((vis_i.*vis_j)>0);
    xiML=xiML(vis,:);
    xjML=xjML(vis,:);
    idxN=idxN(vis,:);
    diV=diV.pick(vis);
    ziV3=Var.hstack(ziV3.pick(vis,vis*0+1),ziV3.pick(vis,vis*0+2),ziV3.pick(vis,vis*0+2));
%      i;
     
    zjV3 = Var.repeat(Z.pick(idxN),1,3);    
    Mdl.constraint(Expr.hstack(diV,Expr.sub(Expr.mulElm(ziV3,xiML),Expr.mulElm(zjV3,xjML))), Domain.inQCone());
end

%%
Mdl.constraint(Expr.sum(D),Domain.equalsTo(1));
Mdl.objective(ObjectiveSense.Maximize, Expr.sum(Z));
%time(rep)=ts;

Mdl.solve();
mu = reshape(Z.level(),N,M)';
D  = reshape(D.level(),N,L-1)';
end

