function [ scenelistout ] = getsceneCombinations( scenelist, numviews)
% Get the new combinations of scenelist.

% Get combinations
cbns = combnk(scenelist,numviews);
numcomb = size(cbns,1);
scenelistout = cbns;

if size(scenelistout,1)>3
    scenelistout = scenelistout(randperm(numcomb,3),:);
end

end