function [ W, S0 ] = convDataFac( m, Pgth )
% convert data for Factorization based NRSfM


N = length(m(1).m);
numframes = length(m);
W = struct2array(m);
W = reshape(W,N*2,[]);
W = W';
Wx = W(:,1:2:end);
Wy = W(:,2:2:end);
W = reshape([Wx(:) Wy(:)]',2*numframes,[]);

if nargin<2
    S0 = 0;
else

    S0 = struct2array(Pgth);
    S0 = reshape(S0,N*3,[]);
    S0 = S0';
    S0x = S0(:,1:3:end);
    S0y = S0(:,2:3:end);
    S0z = S0(:,3:3:end);

    S0 = reshape([S0x(:) S0y(:) S0z(:)]',3*numframes,[]);
end

end

