function [ Pg ] = getgroundtruth( p, paths )
% get ground truth from the barycentrics.
% fixed values:
height = 540;
width = 960;

% get first groundtruth:
file1 = [paths{1},paths{2}];
load(file1);
indices = P.B(:,:,1);
a1 = P.B(:,:,2);
a2 = P.B(:,:,3);
a3 = P.B(:,:,4);

% nonan_ind = find(~isnan(p(1,:)));
nonan_flag = ~isnan(p(1,:));

% get indices for image points:
indpts = sub2ind([height,width],round(p(2,nonan_flag))',round(p(1,nonan_flag))');

% use indices to get the barycentrics:
indices = indices(indpts);
a1 = a1(indpts);
a2 = a2(indpts);
a3 = a3(indpts);

% check if some face indices are incorrect (<1)
facesvalid = (indices>=1);
a1 = a1(facesvalid);
a2 = a2(facesvalid);
a3 = a3(facesvalid);

faces_used = P.faces(indices(facesvalid),:);

% get vertices for the image points in 3D
vertices = P.vertexPos(faces_used(:,1),:).*[a1 a1 a1]+ ...
               P.vertexPos(faces_used(:,2),:).*[a2 a2 a2]+ ...
               P.vertexPos(faces_used(:,3),:).*[a3 a3 a3];
vertices_all = zeros(length(indices),3);
vertices_all(facesvalid,:) = vertices;
                      
P = vertices_all';
Pg = NaN(3,length(indpts));
Pg(:,nonan_flag) = P;

end