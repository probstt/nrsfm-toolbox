function [ n1gth, Q1 ] = computeNormals( m1, Q, er )

nC = 28;
umin=min(m1(1,:))-0.1;umax=max(m1(1,:))+0.1;
vmin=min(m1(2,:))-0.1;vmax=max(m1(2,:))+0.1;
bbs3D = bbs_create(umin, umax, nC, vmin, vmax, nC, 3);
coloc3D = bbs_coloc(bbs3D, m1(1,:), m1(2,:));
lambdas = er*ones(nC-3, nC-3);
bending3D = bbs_bending(bbs3D, lambdas);
ctrlpts3D = (coloc3D'*coloc3D + bending3D) \ (coloc3D'*Q');
ctrlpts3D = ctrlpts3D';

Q1=bbs_eval(bbs3D, ctrlpts3D, m1(1,:)', m1(2,:)',0,0);
% err = sqrt(mean((Q(:) - Q1(:)).^2))

n1x=bbs_eval(bbs3D, ctrlpts3D, m1(1,:)', m1(2,:)',1,0);
n1y=bbs_eval(bbs3D, ctrlpts3D, m1(1,:)', m1(2,:)',0,1);
n1gth=zeros(3,size(m1,2));

for i=1:size(n1x,2)
    nx=n1x(:,i);
    nx=nx./norm(nx);
    ny=n1y(:,i);
    ny=ny./norm(ny);
    nn=-cross(nx,ny);
    nn=nn./norm(nn);
    n1gth(:,i)=nn;
    %n1(:,i)=-sign(nn).*abs(n1(:,i));
end

end

