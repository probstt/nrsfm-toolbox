%
%Dai, Y., Li, H., He, M.: A simple prior-free method for non-rigid structure-from-motion factorization. In: CVPR. (2012)
%
%
function data=dlh(data,opts)

%Get data
for k=1:length(data)
   mMat(k).m=data(k).x2d(1:2,:); 
end

N = length(mMat(1).m);
M = length(mMat);

%Get parameters
K = opts.K; 
rotStruct = opts.rotStruct;

if (K*3)>M
    fprintf('More than %d views are required.\n',K*3)
    return
end
    
[W, S1] = convDataFac(mMat);


 %% start DLH12
tic;
[Shat_BMM Shat_PI Shat_Smooth Rsh R_Recover Shape_Err_BMM Shape_Err_PI Shape_Err_Smooth Rotation_Err] = NRSFM_BMM(W,K,rotStruct);
ts=toc;

for k = 1:length(data)
    kk = 3*k - [2 1 0];  
    data(k).x3d=Shat_BMM(kk,:)
    data(k).x3d_smooth=Shat_Smooth(kk,:);
end

end
