global nrsfm_toolbox_setup

if nrsfm_toolbox_setup==1, return, end
%% Setup solvers

% MOSEK
path = '/path/to/mosek'
system(['export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:' path '/mosek/8/tools/platform/linux64x86/bin/libmosek64.so.8.0']);
addpath([path '/mosek/8/toolbox/r2014a/']);
javaaddpath([path '/mosek/8/tools/platform/linux64x86/bin/mosekmatlab.jar']); 

% CVX
run('path/to/cvx/cvx_setup.m');
% SEDUMI
addpath('path/to/sedumi');
% YALMIP
addpath(genpath('path/to/YALMIP-master'))

%% Geodesic lib
addpath(genpath('path/to/Toolbox-Fast-Marching-Unix/'))
global geodesic_library;
geodesic_library = 'geodesic_matlab_api';      %"release" is faster and "debug" does additional checks

%% Set path
addpath(genpath('.'));

%% Plotting
set(groot, 'defaultTextInterpreter','latex');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex');

nrsfm_toolbox_setup=1;