%% Setup solvers
setup()

%% Load dataset

% Downsample for complex methods
opts.sv=3;          %subsample pts
opts.sp=5;          %subsample views
opts.normalizeK=1;  %normalize with known intrinsics

% Hulk example
dataset=prepare_hulk(opts);

% Flag example
% dataset=prepare_flag(opts);

% Tshirt
% dataset=prepare_tshirt(opts);


data=dataset.data;
imSize=dataset.imageSize;
N=dataset.N;
M=dataset.M;


%% Set reconstruction parameters
options=getDefaultOptions();

%% Reconstruct with desired method

% rec_tlmdh=tlmdh(data,options.tlmdh);
% rec_tlmdh_incr=tlmdh_incr(data,options.tlmdh_incr);
% data=MaxRig(data,options.maxrig);
% rec_dlh=dlh(data,options.dlh);
rec_isom=isom(data,options.isom);

%% Evaluate reconstruction
% 
% [res_tlmdh, ~,err3d_tlmdh]=evaluate(rec_tlmdh,1:M,options.tlmdh);
% mean(err3d_tlmdh)


% [res_tlmdh_incr, ~,err3d_tlmdh_incr]=evaluate(rec_tlmdh_incr,1:M,options.tlmdh);
% mean(err3d_tlmdh_incr)


[res_isom, ~,err3d_isom]=evaluate(rec_isom,1:M,options.isom);
mean(err3d_isom)


%% Visualize results

figure(1)   
visualize(res_isom,1:M);




