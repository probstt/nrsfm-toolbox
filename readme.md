# NRSfM Toolbox 

A common MATLAB toolbox to compare non-rigid shape-from-motion approaches. 
The toolbox provides framework with a uniform data format and wrappers for methods to enable easy comparison with a few lines of code.


## Requirements

Install the following tools, and set the paths in setup.m:

- [MOSEK 8](https://www.mosek.com/downloads/)
- [CVX 2.1](http://cvxr.com/cvx/download/)
- [SeDuMi 1.3](http://sedumi.ie.lehigh.edu/?page_id=58)
- [Yalmip](https://yalmip.github.io/download/)
- [Fast Marching Toolbox](https://ch.mathworks.com/matlabcentral/fileexchange/6110-toolbox-fast-marching)


## Included Methods

- Maximum Depth Heuristic (MDH)- based NRSfM using SOCP [(Chhatkuli et al., 2016 CVPR)](http://openaccess.thecvf.com/content_cvpr_2016/html/Chhatkuli_Inextensible_Non-Rigid_Shape-From-Motion_CVPR_2016_paper.html)
    - Template-based and template-less reconstruction  


- MDH-based incremental densification with unknown focal length [(Probst et al., 2018 ECCV)](https://arxiv.org/abs/1808.04181)
    - Incremental densification
    - Template-based and template-less focal length estimation
    - Template-based intrinsics recovery

- [Maximizing Rigidity](https://github.com/panji1990/Maximizing-rigidity-revisited) [(Ji et al., 2017 ICCV)](http://openaccess.thecvf.com/content_ICCV_2017/papers/Ji_Maximizing_Rigidity_Revisited_ICCV_2017_paper.pdf)

- [DLH Low Rank Factorization](https://github.com/jvlmdr/low-rank-nrsfm/tree/master/src/dai-2012) [(Dai et al., 2012 CVPR)](http://users.cecs.anu.edu.au/~hongdong/CVPR12_Nonrigid_CRC_17_postprint.pdf)

- [Iso-NRSfM](http://igt.ip.uca.fr/~ab/Research/Local-Iso-NRSfM_v1p1.zip) [(Parashar et al., 2018 TPAMI)](http://openaccess.thecvf.com/content_cvpr_2016/papers/Parashar_Isometric_Non-Rigid_Shape-From-Motion_CVPR_2016_paper.pdf)


## Datasets 

- Hulk [(Chhatkuli et al., 2014 BMVC)](http://www.bmva.org/bmvc/2014/papers/paper041/index.html)
- Flag [(White et al., 2007 SIGGRAPH)](http://ryanmwhite.com/data/)
- T-Shirt [(Chhatkuli et al., 2014 BMVC)](http://www.bmva.org/bmvc/2014/papers/paper041/index.html)


## Usage

- The complete set of options and their descriptions for all methods can be found in *getDefaultOptions()*

- Load a dataset using *prepare_xxxx(ds_opts)*. Yields a struct with (normalized) image points, 3d ground truth, calibration, and a 3d template.
    - ds_opts.sv: subsample views
    - ds_opts.sp: subsample points
    - ds_opts.normalizeK: multiply image points with K_inv, if true
    
- Call desired reconstruction/calibration method using the corresponding options
    - e.g. *rec_tlmdh_incr=tlmdh_incr(data,options.tlmdh_incr);*
    
- Call evaluation function in same manner
    - e.g. *[res_tlmdh_incr, ~,err3d_tlmdh_incr]=evaluate(rec_tlmdh_incr,1:M,options.tlmdh_incr)*
    
- A simple visualization is offered by *visualize(res_tlmdh_incr,1:M);*  
    