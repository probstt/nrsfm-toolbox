%% Setup solvers
setup()

%% Load dataset

opts.sv=1;          %subsample pts
opts.sp=1;          %subsample views
opts.normalizeK=0;  %normalize with known intrinsics

% Hulk example
dataset=prepare_hulk(opts);

% Flag example
% dataset=prepare_flag(opts);

% Tshirt
% dataset=prepare_tshirt(opts);


data=dataset.data;
imSize=dataset.imageSize;
N=dataset.N;
M=dataset.M;

%% Set parameters & calibrate
options=getDefaultOptions();
opts=options.tlmdh_incr;
opts.incrSteps=1;

opts.calibration.maxIter=15;
opts.calibration.sweep.f_step=mean(imSize)*0.05;
opts.calibration.sweep.nSteps=5;


calib=tlmdh_calibrate(dataset,opts);
calib


